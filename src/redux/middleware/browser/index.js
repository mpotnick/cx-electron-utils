import { remote } from 'electron';
import {
  SET_IS_LOADING,
  setNavigationState,
  GO,
  GO_BACK,
  GO_FORWARD,
  MAXIMIZE_WINDOW,
  MINIMIZE_WINDOW,
  RESTORE_WINDOW,
  CLOSE_WINDOW,
} from '../../modules/browser';

export function loadWatcher({ dispatch, getState }) {
  return next => action => {
    if (action.type === SET_IS_LOADING
      && action.payload.isLoading === false
    ) {
      const { webContents } = getState().browser;
      next(setNavigationState({
        activeIndex: webContents.getActiveIndex(),
        url: webContents.getURL(),
        canGoBack: webContents.canGoBack(),
        canGoForward: webContents.canGoForward(),
      }));
    }
    return next(action);
  };
};

export function goWatcher({ dispatch, getState }) {
  return next => action => {
    if (action.type === GO
      || action.type === GO_BACK
      || action.type === GO_FORWARD
    ) {
      const { webContents } = getState().browser;
      switch (action.type) {
        case GO:
          if (webContents.canGoToOffset(action.payload)) {
            webContents.goToOffset(action.payload);
          } else {
            // todo show error
            // return next(showError);
          }
          break
        case GO_FORWARD:
          if (webContents.canGoForward()) {
            webContents.goForward();
          } else {
            // todo show error
            // return next(showError);
          }
          break;
        case GO_BACK:
          if (webContents.canGoBack()) {
            webContents.goBack();
          } else {
            // todo show error
            // return next(showError);
          }
          break;
        default:
          return next(action);
      }
    }
    return next(action);
  };
};

export function windowWatcher({ dispatch, getState }) {
  return next => action => {
    if (action.type === MAXIMIZE_WINDOW
      || action.type === MINIMIZE_WINDOW
      || action.type === RESTORE_WINDOW
      || action.type === CLOSE_WINDOW
    ) {
      const win = remote.BrowserWindow.fromWebContents(getState().browser.webContents.hostWebContents);
      if (win) {
        switch (action.type) {
          case MAXIMIZE_WINDOW:
            win.maximize();
            break;
          case MINIMIZE_WINDOW:
            win.minimize();
            break;
          case RESTORE_WINDOW:
            if (win.isMaximized()) {
              win.unmaximize();
            } else if (win.isMinimized()) {
              win.restore();
            }
            break;
          case CLOSE_WINDOW:
            win.close();
            break;
          default:
            return next(action);
        }
      }
    }
    return next(action);
  };
};
