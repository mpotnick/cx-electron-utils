import { isColorLight } from '../../../utils/color';

const themeColor = '#00aeff';

export function getWindowState(value) {
  if (value.isMaximized()) {
    return 'MAXIMIZED';
  } else if (value.isMinimized()) {
    return 'MINIMIZED';
  }
  return 'NORMAL';
}

export default {
  title: '',
  canGoBack: false,
  canGoForward: false,
  isLoading: false,
  themeColor,
  isThemeLight: isColorLight(themeColor),
  windowState: 'NORMAL',
  windowWidth: undefined,
  windowHeight: undefined,
  webContents: undefined,
  activeIndex: 0,
};
