import color from 'color';
import { isColorLight } from '../../../utils/color';
import {
  createAction,
  createErrorAction,
  createSetter,
  createReducer,
} from 'cx-redux-utils';

import defaultState from './state';

export const SET_TITLE = '@@cxElectronUtils/browser/setTitle';
export const setTitle = createAction(SET_TITLE);
function setTitleHandler(state, action) {
  return {
    title: action.payload,
  };
}

export const SET_THEME_COLOR = '@@cxElectronUtils/browser/setThemeColor';
export const setThemeColor = createAction(SET_THEME_COLOR);
function setThemeColorHandler(state, action) {
  try {
    const c = color(action.payload).hexString();;
    return {
      themeColor: c,
      isThemeLight: isColorLight(c),
    };
  } catch (e) {
    const { themeColor, isThemeLight } = state;
    return {
      themeColor,
      isThemeLight,
    };
  }
}

export const GO_FORWARD = '@@cxElectronUtils/browser/goForward';
export const goForward = createAction(GO_FORWARD);

export const GO_BACK = '@@cxElectronUtils/browser/goBack';
export const goBack = createAction(GO_BACK);

export const GO = '@@cxElectronUtils/browser/go';
export const go = createAction(GO);


export const SET_IS_LOADING = '@@cxElectronUtils/browser/setIsLoading';
export const setIsLoading = createAction(SET_IS_LOADING);
function setIsLoadingHandler(state, action) {
  const { isLoading } = action.payload;
  return {
    isLoading,
  };
}

export const SET_NAVIGATION_STATE = '@@cxElectronUtils/browser/setNavigationState';
export const setNavigationState = createAction(SET_NAVIGATION_STATE);
function setNavigationStateHandler(state, action) {
  const { activeIndex, canGoBack, canGoForward, url } = action.payload;
  return {
    activeIndex,
    canGoBack,
    canGoForward,
    url,
  };
}

export const SET_WEB_CONTENTS = '@@cxElectronUtils/browser/setWebContents';
export const setWebContents = createAction(SET_WEB_CONTENTS);
function setWebContentsHandler(state, action) {
  const { webContents } = action.payload;
  return {
    webContents,
  };
}

export const SET_WINDOW_SIZE = '@@cxElectronUtils/window/setSize';
export const setWindowSize = createAction(SET_WINDOW_SIZE);
function setWindowSizeHandler(state, action) {
  const { windowWidth, windowHeight } = action.payload;
  return {
    windowWidth,
    windowHeight,
  };
}

export const MINIMIZE_WINDOW = '@@cxElectronUtils/window/minimize';
export const minimizeWindow = createAction(MINIMIZE_WINDOW);
function minimizeWindowHandler(state, action) {
  return {
    windowState: 'MINIMIZED',
  };
}

export const MAXIMIZE_WINDOW = '@@cxElectronUtils/window/maximize';
export const maximizeWindow = createAction(MAXIMIZE_WINDOW);
function maximizeWindowHandler(state, action) {
  return {
    windowState: 'MAXIMIZED',
  };
}

export const RESTORE_WINDOW = '@@cxElectronUtils/window/restore';
export const restoreWindow = createAction(RESTORE_WINDOW);
function restoreWindowHandler(state, action) {
  return {
    windowState: 'NORMAL',
  };
}

export const CLOSE_WINDOW = '@@cxElectronUtils/window/close';
export const closeWindow = createAction(CLOSE_WINDOW);

export const actionMap = {
  [SET_TITLE]: setTitleHandler,
  [SET_THEME_COLOR]: setThemeColorHandler,
  [SET_IS_LOADING]: setIsLoadingHandler,
  [SET_NAVIGATION_STATE]: setNavigationStateHandler,
  [SET_WEB_CONTENTS]: setWebContentsHandler,
  [SET_WINDOW_SIZE]: setWindowSizeHandler,
  [MINIMIZE_WINDOW]: minimizeWindowHandler,
  [MAXIMIZE_WINDOW]: maximizeWindowHandler,
  [RESTORE_WINDOW]: restoreWindowHandler,
};

export const browser = createReducer(defaultState, actionMap);
