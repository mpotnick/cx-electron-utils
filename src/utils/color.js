import color from 'color';

export const isColorLight = function(c){
  try {
    return color(c).luminosity() > 0.5;
  } catch (e) {
    //todo
  }
};

export const isColorDark = function(c){
  try {
    return color(c).luminosity() <= 0.5;
  } catch (e) {
    //todo
  }
};