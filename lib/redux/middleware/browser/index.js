'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadWatcher = loadWatcher;
exports.goWatcher = goWatcher;
exports.windowWatcher = windowWatcher;

var _electron = require('electron');

var _browser = require('../../modules/browser');

function loadWatcher(_ref) {
  var dispatch = _ref.dispatch;
  var getState = _ref.getState;

  return function (next) {
    return function (action) {
      if (action.type === _browser.SET_IS_LOADING && action.payload.isLoading === false) {
        var webContents = getState().browser.webContents;

        next((0, _browser.setNavigationState)({
          activeIndex: webContents.getActiveIndex(),
          url: webContents.getURL(),
          canGoBack: webContents.canGoBack(),
          canGoForward: webContents.canGoForward()
        }));
      }
      return next(action);
    };
  };
};

function goWatcher(_ref2) {
  var dispatch = _ref2.dispatch;
  var getState = _ref2.getState;

  return function (next) {
    return function (action) {
      if (action.type === _browser.GO || action.type === _browser.GO_BACK || action.type === _browser.GO_FORWARD) {
        var webContents = getState().browser.webContents;

        switch (action.type) {
          case _browser.GO:
            if (webContents.canGoToOffset(action.payload)) {
              webContents.goToOffset(action.payload);
            } else {
              // todo show error
              // return next(showError);
            }
            break;
          case _browser.GO_FORWARD:
            if (webContents.canGoForward()) {
              webContents.goForward();
            } else {
              // todo show error
              // return next(showError);
            }
            break;
          case _browser.GO_BACK:
            if (webContents.canGoBack()) {
              webContents.goBack();
            } else {
              // todo show error
              // return next(showError);
            }
            break;
          default:
            return next(action);
        }
      }
      return next(action);
    };
  };
};

function windowWatcher(_ref3) {
  var dispatch = _ref3.dispatch;
  var getState = _ref3.getState;

  return function (next) {
    return function (action) {
      if (action.type === _browser.MAXIMIZE_WINDOW || action.type === _browser.MINIMIZE_WINDOW || action.type === _browser.RESTORE_WINDOW || action.type === _browser.CLOSE_WINDOW) {
        var win = _electron.remote.BrowserWindow.fromWebContents(getState().browser.webContents.hostWebContents);
        if (win) {
          switch (action.type) {
            case _browser.MAXIMIZE_WINDOW:
              win.maximize();
              break;
            case _browser.MINIMIZE_WINDOW:
              win.minimize();
              break;
            case _browser.RESTORE_WINDOW:
              if (win.isMaximized()) {
                win.unmaximize();
              } else if (win.isMinimized()) {
                win.restore();
              }
              break;
            case _browser.CLOSE_WINDOW:
              win.close();
              break;
            default:
              return next(action);
          }
        }
      }
      return next(action);
    };
  };
};