'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getWindowState = getWindowState;

var _color = require('../../../utils/color');

var themeColor = '#00aeff';

function getWindowState(value) {
  if (value.isMaximized()) {
    return 'MAXIMIZED';
  } else if (value.isMinimized()) {
    return 'MINIMIZED';
  }
  return 'NORMAL';
}

exports.default = {
  title: '',
  canGoBack: false,
  canGoForward: false,
  isLoading: false,
  themeColor: themeColor,
  isThemeLight: (0, _color.isColorLight)(themeColor),
  windowState: 'NORMAL',
  windowWidth: undefined,
  windowHeight: undefined,
  webContents: undefined,
  activeIndex: 0
};