'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.browser = exports.actionMap = exports.closeWindow = exports.CLOSE_WINDOW = exports.restoreWindow = exports.RESTORE_WINDOW = exports.maximizeWindow = exports.MAXIMIZE_WINDOW = exports.minimizeWindow = exports.MINIMIZE_WINDOW = exports.setWindowSize = exports.SET_WINDOW_SIZE = exports.setWebContents = exports.SET_WEB_CONTENTS = exports.setNavigationState = exports.SET_NAVIGATION_STATE = exports.setIsLoading = exports.SET_IS_LOADING = exports.go = exports.GO = exports.goBack = exports.GO_BACK = exports.goForward = exports.GO_FORWARD = exports.setThemeColor = exports.SET_THEME_COLOR = exports.setTitle = exports.SET_TITLE = undefined;

var _actionMap;

var _color = require('color');

var _color2 = _interopRequireDefault(_color);

var _color3 = require('../../../utils/color');

var _cxReduxUtils = require('cx-redux-utils');

var _state = require('./state');

var _state2 = _interopRequireDefault(_state);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SET_TITLE = exports.SET_TITLE = '@@cxElectronUtils/browser/setTitle';
var setTitle = exports.setTitle = (0, _cxReduxUtils.createAction)(SET_TITLE);
function setTitleHandler(state, action) {
  return {
    title: action.payload
  };
}

var SET_THEME_COLOR = exports.SET_THEME_COLOR = '@@cxElectronUtils/browser/setThemeColor';
var setThemeColor = exports.setThemeColor = (0, _cxReduxUtils.createAction)(SET_THEME_COLOR);
function setThemeColorHandler(state, action) {
  try {
    var c = (0, _color2.default)(action.payload).hexString();;
    return {
      themeColor: c,
      isThemeLight: (0, _color3.isColorLight)(c)
    };
  } catch (e) {
    var themeColor = state.themeColor;
    var isThemeLight = state.isThemeLight;

    return {
      themeColor: themeColor,
      isThemeLight: isThemeLight
    };
  }
}

var GO_FORWARD = exports.GO_FORWARD = '@@cxElectronUtils/browser/goForward';
var goForward = exports.goForward = (0, _cxReduxUtils.createAction)(GO_FORWARD);

var GO_BACK = exports.GO_BACK = '@@cxElectronUtils/browser/goBack';
var goBack = exports.goBack = (0, _cxReduxUtils.createAction)(GO_BACK);

var GO = exports.GO = '@@cxElectronUtils/browser/go';
var go = exports.go = (0, _cxReduxUtils.createAction)(GO);

var SET_IS_LOADING = exports.SET_IS_LOADING = '@@cxElectronUtils/browser/setIsLoading';
var setIsLoading = exports.setIsLoading = (0, _cxReduxUtils.createAction)(SET_IS_LOADING);
function setIsLoadingHandler(state, action) {
  var isLoading = action.payload.isLoading;

  return {
    isLoading: isLoading
  };
}

var SET_NAVIGATION_STATE = exports.SET_NAVIGATION_STATE = '@@cxElectronUtils/browser/setNavigationState';
var setNavigationState = exports.setNavigationState = (0, _cxReduxUtils.createAction)(SET_NAVIGATION_STATE);
function setNavigationStateHandler(state, action) {
  var _action$payload = action.payload;
  var activeIndex = _action$payload.activeIndex;
  var canGoBack = _action$payload.canGoBack;
  var canGoForward = _action$payload.canGoForward;
  var url = _action$payload.url;

  return {
    activeIndex: activeIndex,
    canGoBack: canGoBack,
    canGoForward: canGoForward,
    url: url
  };
}

var SET_WEB_CONTENTS = exports.SET_WEB_CONTENTS = '@@cxElectronUtils/browser/setWebContents';
var setWebContents = exports.setWebContents = (0, _cxReduxUtils.createAction)(SET_WEB_CONTENTS);
function setWebContentsHandler(state, action) {
  var webContents = action.payload.webContents;

  return {
    webContents: webContents
  };
}

var SET_WINDOW_SIZE = exports.SET_WINDOW_SIZE = '@@cxElectronUtils/window/setSize';
var setWindowSize = exports.setWindowSize = (0, _cxReduxUtils.createAction)(SET_WINDOW_SIZE);
function setWindowSizeHandler(state, action) {
  var _action$payload2 = action.payload;
  var windowWidth = _action$payload2.windowWidth;
  var windowHeight = _action$payload2.windowHeight;

  return {
    windowWidth: windowWidth,
    windowHeight: windowHeight
  };
}

var MINIMIZE_WINDOW = exports.MINIMIZE_WINDOW = '@@cxElectronUtils/window/minimize';
var minimizeWindow = exports.minimizeWindow = (0, _cxReduxUtils.createAction)(MINIMIZE_WINDOW);
function minimizeWindowHandler(state, action) {
  return {
    windowState: 'MINIMIZED'
  };
}

var MAXIMIZE_WINDOW = exports.MAXIMIZE_WINDOW = '@@cxElectronUtils/window/maximize';
var maximizeWindow = exports.maximizeWindow = (0, _cxReduxUtils.createAction)(MAXIMIZE_WINDOW);
function maximizeWindowHandler(state, action) {
  return {
    windowState: 'MAXIMIZED'
  };
}

var RESTORE_WINDOW = exports.RESTORE_WINDOW = '@@cxElectronUtils/window/restore';
var restoreWindow = exports.restoreWindow = (0, _cxReduxUtils.createAction)(RESTORE_WINDOW);
function restoreWindowHandler(state, action) {
  return {
    windowState: 'NORMAL'
  };
}

var CLOSE_WINDOW = exports.CLOSE_WINDOW = '@@cxElectronUtils/window/close';
var closeWindow = exports.closeWindow = (0, _cxReduxUtils.createAction)(CLOSE_WINDOW);

var actionMap = exports.actionMap = (_actionMap = {}, _defineProperty(_actionMap, SET_TITLE, setTitleHandler), _defineProperty(_actionMap, SET_THEME_COLOR, setThemeColorHandler), _defineProperty(_actionMap, SET_IS_LOADING, setIsLoadingHandler), _defineProperty(_actionMap, SET_NAVIGATION_STATE, setNavigationStateHandler), _defineProperty(_actionMap, SET_WEB_CONTENTS, setWebContentsHandler), _defineProperty(_actionMap, SET_WINDOW_SIZE, setWindowSizeHandler), _defineProperty(_actionMap, MINIMIZE_WINDOW, minimizeWindowHandler), _defineProperty(_actionMap, MAXIMIZE_WINDOW, maximizeWindowHandler), _defineProperty(_actionMap, RESTORE_WINDOW, restoreWindowHandler), _actionMap);

var browser = exports.browser = (0, _cxReduxUtils.createReducer)(_state2.default, actionMap);