'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _browser = require('./browser');

var _browser2 = _interopRequireDefault(_browser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  browser: _browser2.default
};