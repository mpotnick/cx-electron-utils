'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _modules = require('./modules');

var _modules2 = _interopRequireDefault(_modules);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  modules: _modules2.default
};