'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isColorDark = exports.isColorLight = undefined;

var _color = require('color');

var _color2 = _interopRequireDefault(_color);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isColorLight = exports.isColorLight = function isColorLight(c) {
  try {
    return (0, _color2.default)(c).luminosity() > 0.5;
  } catch (e) {
    //todo
  }
};

var isColorDark = exports.isColorDark = function isColorDark(c) {
  try {
    return (0, _color2.default)(c).luminosity() <= 0.5;
  } catch (e) {
    //todo
  }
};